import os
import unittest
from flask import Flask
from werkzeug.datastructures import FileStorage
from app import app

class AppTestCase(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

        # Create a temporary directory for test uploads
        self.temp_upload_dir = os.path.join(app.instance_path, 'test_uploads')
        os.makedirs(self.temp_upload_dir, exist_ok=True)

    def tearDown(self):
        # Remove the temporary upload directory and files
        for filename in os.listdir(self.temp_upload_dir):
            file_path = os.path.join(self.temp_upload_dir, filename)
            os.remove(file_path)
        os.rmdir(self.temp_upload_dir)

    def test_home_route(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'upload.html', response.data)

    def test_upload_route(self):
        # Create a temporary test file
        file_path = os.path.join(self.temp_upload_dir, 'test_data.csv')
        with open(file_path, 'w') as file:
            file.write('Test data')

        # Upload the test file
        with open(file_path, 'rb') as file:
            data = {'file': (FileStorage(file), 'test_data.csv')}
            response = self.app.post('/upload/', data=data, content_type='multipart/form-data')

        self.assertEqual(response.status_code, 200)
        self.assertIn(b'uploadSuccess.html', response.data)

    def test_predict_route(self):
        # Create a temporary test file
        file_path = os.path.join(self.temp_upload_dir, 'test_data.csv')
        with open(file_path, 'w') as file:
            file.write('Test data')

        # Upload the test file
        with open(file_path, 'rb') as file:
            data = {'file': (FileStorage(file), 'test_data.csv')}
            response = self.app.post('/predict/', data=data, content_type='multipart/form-data')

        self.assertEqual(response.status_code, 200)
        # Add assertions for the expected JSON response

    def test_predict_model_route(self):
        # Create a temporary test file
        file_path = os.path.join(self.temp_upload_dir, 'test_data.csv')
        with open(file_path, 'w') as file:
            file.write('Test data')

        # Upload the test file
        with open(file_path, 'rb') as file:
            data = {'file': (FileStorage(file), 'test_data.csv')}
            response = self.app.post('/predict/test_model/', data=data, content_type='multipart/form-data')

        self.assertEqual(response.status_code, 200)
        # Add assertions for the expected JSON response

if __name__ == '__main__':
    unittest.main()