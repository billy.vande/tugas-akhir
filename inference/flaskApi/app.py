import joblib
import json
import pandas as pd
import os
from werkzeug.utils import secure_filename
from flask import Flask, request ,render_template, json, redirect, url_for

app = Flask(__name__)

uploads_dir = os.path.join(app.instance_path, 'uploads')
os.makedirs(uploads_dir, exist_ok=True)

@app.route('/',methods=["Get","POST"])
def home():
    return render_template("upload.html")

@app.route('/predict/',methods=['GET','POST'])
def predict():
    uploaded_file = request.files['file']
    df = pd.read_csv(uploaded_file)
    print(df)
    with open("model.pkl", 'rb') as file:
            classifier = joblib.load(file)
    predictions_test = classifier.predict(df)
    predicted_list = []
    predicted_list.append(predictions_test.tolist())
    jsoned = json.dumps(predicted_list)
    # print(jsoned)
    return jsoned

@app.route('/upload/', methods=['GET', 'POST'])
def uploadForm():
    print(request.files)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        uploaded_file.save(os.path.join(uploads_dir, secure_filename(uploaded_file.filename)))
        return render_template("uploadSuccess.html", name = uploaded_file.filename[:-4])
        # return redirect(url_for('foo'))
    return render_template('upload.html')

@app.route('/predict/<string:name>/',methods=['GET','POST'])
def predictModel(name):
    if request.method == 'POST':
        uploaded_file = request.files['file']
        df = pd.read_csv(uploaded_file)
        filename = "instance/uploads/" + name + ".pkl"
        print(filename)
        with open(filename, 'rb') as file:
                classifier = joblib.load(file)
        predictions_test = classifier.predict(df)
        predicted_list = []
        predicted_list.append(predictions_test.tolist())
        jsoned = json.dumps(predicted_list)
        return jsoned
    return render_template('predict.html')

if __name__ == '__main__':
     app.run(debug=True, port=5002)